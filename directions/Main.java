package directions;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		
		/*
		 * How to use JOptionPane:
		 * JOptionPane.showMessageDialog(null, "Hello World!");
		 *   - no matter what, put null, and then the second parameter is the message you want to
		 *   show
		 * 
		 * JOptionPane.showInputDialog("type something");
		 *   - this returns what the user types, so you would have a string variable to 
		 *   save what the user types; the parameter is specifying what the user should type
		 */
		
		/*
		 * Part 1: are you a student or teacher?
		 *  - using JOptionPane, ask the user to type in S for student and T for teacher
		 *  - depending on their response, create either an instance of Student or Teacher
		 *  
		 * Part 2: asking for information and storing that information
		 *  - using JOptionPane, ask for each instance field depending on whether we have Teacher or Student
		 *    - Student: 4 different JOptionPane input dialogs for name, graduation year, gpa, and id
		 *    - Teacher: 5 different JOptionPane input dialogs for name, yearsTeaching, class, numOfStudents, hour
		 *    - with each input, parse it if you need to, and then using your setters, set the instance 
		 *    fields to the value they give
		 *    
		 * Part 3: additional stuff (APPLIES ONLY IF STUDENT)
		 *  - JOptionPane (input) asks: "Would you like to update your GPA with the classes you're currently taking?
		 *  "Type Y for yes and N for No"
		 *  
		 *  - depending on their response, call 'calculateGPA()'
		 *  
		 * Part 4: display information
		 *  - call the 'display()' method and store the string it returns
		 *  - display that string using JOptionPane.showMessageDialog()
		 * 
		 */

	}

}
