package directions;

public class Student {
	
	/*
	 * instance fields:
	 *   - a variable that stores the student's name
	 *   - a variable that stores the student's graduation year
	 *   - a variable that stores the student's GPA (decimal)
	 *   - a variable that stores the student's id (String)
	 *   
	 * constructors:
	 *   - default constructor, set all instance fields to 0's/empty Strings
	 *   - have a constructor that takes parameters for all the instance fields and then assigns them
	 * 
	 * getters and setters for ALL instance fields
	 * 
	 * int calculateGPA()
	 *   - assumptions: they're taking only six classes, the highest gpa is a 4.0
	 *   - JOptionPane: show message that says the following: 
	 *     "Input the number of A's, B's, C's, and D's you have in the six classes you're currently taking"
	 *   - using JOptionPane, ask the user for how many A's they've gotten 
	 *     - each A = 2.0/3.0 (or you could do: 0.666666667)
	 *   - using JOptionPane, ask the user for how many B's they've gotten
	 *     - each B = 0.5 
	 *   - using JOptionPane, ask the user for how many C's they've gotten
	 *     - each C = 1.0/3.0 (or you could do: 0.33333334)
	 *   - using JOptionPane, ask the user for how many D's they've gotten  
	 *     - each D = 0.25 
	 *     
	 *   - update the GPA instance field with it's previous value + the new value divided by 2
	 *   
	 *   return the final GPA
	 *   
	 * display() method, return type is going to be String
	 *   - going to return one BIG FAT string that has all the instance fields and their values
	 *   - a call to display() would return the following:
	 *   "Your name is ___ 
	 *    You're graduating in ____
	 *    Your GPA is ___ 
	 *    Your student ID is ____"
	 *     
	 *  INFO: how to convert a string to an integer
	 *    - Integer.parseInt(string); //turns your String to an int
	 *  INFO: how to convert a string to an integer
	 *    - Double.parseDouble(string); //turns your String to a double
	 */

}
