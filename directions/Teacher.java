package directions;

public class Teacher {

	/*
	 * instance fields:
	 *   - a variable that stores the teacher's name
	 *   - a variable that stores the amount of time this teacher has been teaching
	 *   - a variable that stores the name of the class they're teaching
	 *   - a variable that stores the number of students in their class
	 *   - a variable that stores the period/hour that they teach this class
	 *   
	 * constructors:
	 *   - default constructor, set all instance fields to 0's/empty Strings
	 *   - have a constructor that takes parameters for all the instance fields and then assigns them
	 *   
	 * getters + setters for ALL the instance fields
	 * 
	 * display() method, return type is going to be String
	 *   - going to return one BIG FAT string that has all the instance fields and their values
	 *   - a call to display() would return the following:
	 *   "Your name is ___ 
	 *    You've been teaching for ___ years
	 *    You teach _____ class
	 *    You have ____ students in your class
	 *    You teach during the ___ hour"
	 * 
	 */
	
	

}
