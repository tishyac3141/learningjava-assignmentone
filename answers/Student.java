package answers;

import javax.swing.JOptionPane;

public class Student {

    private String name;
    private int graduationYear;
    private double gpa;
    private String id;

    public Student(String name, int gradYear, double gpa, String id) {
        this.name = name;
        this.graduationYear = gradYear;
        this.gpa = gpa;
        this.id = id;
    }

    public Student() {
        name = "";
        graduationYear = 0;
        gpa = 0.0;
        id = "";
    }

    public double calculateGPA() {
        JOptionPane.showMessageDialog(null,
                "Please tell us how many A's, B's, C's, and D's you have. Be sure to type in just a single-digit number!");
        int numA = Integer.parseInt(JOptionPane.showInputDialog(null, "How many A's do you have?"));
        int numB = Integer.parseInt(JOptionPane.showInputDialog(null, "How many B's do you have?"));
        int numC = Integer.parseInt(JOptionPane.showInputDialog(null, "How many C's do you have?"));
        int numD = Integer.parseInt(JOptionPane.showInputDialog(null, "How many D's do you have?"));

        double newGPA = (numA * (2.0 / 3.0)) + (numB * (0.5)) + (numC * (1.0 / 3.0)) + (numD * 0.25);
        gpa = (gpa + newGPA) / 2.0;

        return gpa;

    }

    public String display() {
        String studentName = "Your name is " + name;
        String gradYear = "You're in the class of " + graduationYear;
        String GPA = "Your current GPA is " + String.format("%.2f", gpa);
        String ID = "Your student id is " + id;

        return studentName + "\n" + gradYear + "\n" + GPA + "\n" + ID;
    }

    public void setGraduationYear(int gradYear) {
        this.graduationYear = gradYear;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setGPA(double newGPA) {
        this.gpa = newGPA;
    }

    public void setID(String newID) {
        this.id = newID;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public String getName() {
        return name;
    }

    public double getGPA() {
        return gpa;
    }

    public String getID() {
        return id;
    }

}
