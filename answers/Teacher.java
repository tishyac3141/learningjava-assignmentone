package answers;

public class Teacher {
    private String name;
    private int yearsTeaching;
    private String className;
    private int numOfStudents;
    private int period;

    public Teacher() {
		name = "";
		yearsTeaching = 0;
		className = "";
		numOfStudents = 0;
		period = 0;
	}

    public Teacher(String name, int yearsTeaching, String className, int numOfStudents, int period) {
		this.name = name;
		this.yearsTeaching = yearsTeaching;
		this.className = className;
		this.numOfStudents = numOfStudents;
		this.period = period;
	}

    public String display() {
        String teacherName = "Your name is " + name;
        String years = "You've been teaching for " + yearsTeaching + " years";
        String nameOfClass = "You teach " + className + " class";
        String students = "You have " + numOfStudents + " students in your class";
        String hour = "You teach during hour " + period;

        return teacherName + "\n" + years + "\n" + nameOfClass + "\n" + students + "\n" + hour;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setYearsTeaching(int years){
        this.yearsTeaching = years;
    }

    public void setClassName(String subject){
        this.className = subject;
    }

    public void setNumOfStudents(int students){
        this.numOfStudents = students;
    }

    public void setHour(int period){
        this.period = period;
    }

    public String getName(){
        return name;
    }

    public int getYearsTeaching(){
        return yearsTeaching;
    }

    public String getClassName(){
        return className;
    }

    public int getHour(){
        return period;
    }

    public int getNumOfStudents(){
        return numOfStudents;
    }
}
