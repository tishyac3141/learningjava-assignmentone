package answers;

import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {
        Teacher teacher = null;
        Student student = null;

        JOptionPane.showMessageDialog(null, "Welcome! Please give us some information about yourself!");
        String type = JOptionPane.showInputDialog(null,
                "Are you a student or a teacher? Please type S for student and T for teacher.");

        while (teacher == null && student == null) {
            if (type.equals("S")) {
                student = new Student();
                break;
            } else if (type.equals("T")) {
                teacher = new Teacher();
                break;
            } else {
                type = JOptionPane.showInputDialog(null, "Error! Please type in S for student or T for teacher.");
            }
        }

        if (student != null) {
            student.setName(JOptionPane.showInputDialog(null, "What is your name?"));
            student.setGraduationYear(
                    Integer.parseInt(JOptionPane.showInputDialog(null, "What year are you graduating in?")));
            student.setID(JOptionPane.showInputDialog(null, "What is your student ID?"));
            student.setGPA(
                    Double.parseDouble(JOptionPane.showInputDialog(null, "What is your current GPA on a 4.0 scale?")));

            String answer = JOptionPane.showInputDialog(null,
                    "If you're taking 6 classes, would you like to get an estimate for what your gpa will be with your current grades? Type Y for yes and N for no");
            if (answer.equals("Y")) {
                double gpa = student.calculateGPA();
                JOptionPane.showMessageDialog(null, "With your current grades, you will have a " + String.format("%.2f", gpa) + " GPA.");
                String confirm = student.display();
                JOptionPane.showMessageDialog(null, "Here is the information we have about you: " + confirm);
                JOptionPane.showMessageDialog(null, "Thank you! Have a nice day!");
            } else if (answer.equals("N")) {
                String confirm = student.display();
                JOptionPane.showMessageDialog(null, "Alright! Here is the information we have about you: \n" + confirm);
                JOptionPane.showMessageDialog(null, "Thank you! Have a nice day!");
            } else {
                JOptionPane.showMessageDialog(null,
                        "Error! That was an invalid answer. We'll just assume that's a no.");
                String confirm = student.display();
                JOptionPane.showMessageDialog(null, "Here is the information we have about you: \n" + confirm);
                JOptionPane.showMessageDialog(null, "Thank you! Have a nice day!");
            }

        } else {
            teacher.setName(JOptionPane.showInputDialog(null, "What is your name?"));
            teacher.setYearsTeaching(
                    Integer.parseInt(JOptionPane.showInputDialog(null, "How many years have you been teaching?")));
            teacher.setClassName(JOptionPane.showInputDialog(null, "What subject do you teach?"));
            teacher.setHour(Integer
                    .parseInt(JOptionPane.showInputDialog(null, "What hour do you teach " + teacher.getClassName())));
            teacher.setNumOfStudents(Integer.parseInt(JOptionPane.showInputDialog(null,
                    "How many students are in your " + teacher.getClassName() + " class?")));

            String confirm = teacher.display();
            JOptionPane.showMessageDialog(null, "Here is the information we have about you: \n" + confirm);
            JOptionPane.showMessageDialog(null, "Thank you! Have a nice day!");

        }
    }

}
